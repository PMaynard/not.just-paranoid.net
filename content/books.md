---
title: Books
---

A list of books that I've read or are currently reading.  
In order of roughly when I started them.  

2020

- Cryptography Engineering: Design Principles and Practical Applications Niels Ferguson, Bruce Schneier, Tadayoshi Kohno 
- Applied Cryptography: Protocols, Algorithms and Source Code in C, 20th Anniversary Edition by [Bruce Schneier](https://en.wikipedia.org/wiki/Bruce_Schneier)
- The Book of PF by Peter N. M. Hansteen
- Absolute OpenBSD by Michael W. Lucas
- Sapiens: A Brief History of Humankind by [Yuval Noah Harari](https://en.wikipedia.org/wiki/Yuval_Noah_Harari)
- The Shortest History of Germany by James Hawes 
- The Future of the Mind by [Michio Kaku](https://en.wikipedia.org/wiki/Michio_Kaku)
- Altered Carbon by Richard K. Morgan 
- Absolute FreeBSD by Michael W. Lucas
- FreeBSD Mastery ZFS by Michael W. Lucas
- The Future of Humanity by [Michio Kaku](https://en.wikipedia.org/wiki/Michio_Kaku)
- Arrival by Ted Chiang
- [Radicals Chasing Utopia]() by [Jamie Bartlett](https://en.wikipedia.org/wiki/Jamie_Bartlett_(journalist)) 
- The Design and Implementation of the FreeBSD Operating System by  Marshall Kirk McKusick and George V. Neville-Neil

2019

- [Open Veins of Latin America](https://en.wikipedia.org/wiki/Open_Veins_of_Latin_America) by [Eduardo Galeano](https://en.wikipedia.org/wiki/Eduardo_Galeano)
- The Moon is a Harsh Mistress by Robert A. Heinlein 
- Dune by [Frank Herbert](https://en.wikipedia.org/wiki/Frank_Herbert)
- [We](https://en.wikipedia.org/wiki/We_%28novel%29) by [Yevgeny Zamyatin](https://en.wikipedia.org/wiki/Yevgeny_Zamyatin)
- Book of the Moon by [Dr. Maggie Aderin-Pocock](https://en.wikipedia.org/wiki/Maggie_Aderin-Pocock)

2018

- Ishmael by Daniel Quinn 

2017 

- Homo Deus: A Brief History of Tomorrow by [Yuval Noah Harari](https://en.wikipedia.org/wiki/Yuval_Noah_Harari)

2015 

- [Nineteen Eighty-Four](https://en.wikipedia.org/wiki/Nineteen_Eighty-Four) by [George Orwell](https://en.wikipedia.org/wiki/George_Orwell)
- [Animal Farm](https://en.wikipedia.org/wiki/Animal_Farm) by [George Orwell](https://en.wikipedia.org/wiki/George_Orwell)
- [Foundation series](https://en.wikipedia.org/wiki/Foundation_series) by [Isacc Asimov](https://en.wikipedia.org/wiki/Isaac_Asimov)

This page is still a work in progress.