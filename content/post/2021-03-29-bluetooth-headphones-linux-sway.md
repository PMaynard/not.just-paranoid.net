---
title: "Bluetooth Headphones, Linux, and Sway"
date: 2021-03-29T11:17:15+01:00
---

Quick steps to get Bluetooth headphones media controls working under Linux and the sway windows manager. This works for Media being played from Firefox. 

## Requirements 

- [playerctl](https://github.com/altdesktop/playerctl), Control media players via [MPRIS](https://specifications.freedesktop.org/mpris-spec/latest/). 
- [mpris-proxy](https://git.kernel.org/pub/scm/bluetooth/bluez.git/tree/tools/mpris-proxy.c), Bluetooth to MPRIS proxy. 

PlayerCTL is typically found in distributions' packages. While mpris-proxy is a tool part of the BlueZ Bluetooth stack in the Linux Kernel, so hopefully you'll have it already.

## Sway Key Bindings

Add the following key bindings to your sway config: 

	bindsym XF86AudioPlay exec playerctl play-pause
	bindsym XF86AudioNext exec playerctl next
	bindsym XF86AudioPrev exec playerctl previous

Reload the sway config.

## Start mpris-proxy

Start mpris-proxy in your terminal: 

	$ mpris-proxy 
	org.bluez appeared
	Bluetooth Adapter /org/bluez/hci0 found
	player org.mpris.MediaPlayer2.firefox.instance123934 at :1.906 found
	Bluetooth Transport /org/bluez/hci0/dev_94_DB_56_53_B7_41/sep5/fd12 found

Test your headset media controls, if it works have the proxy start on boot and you're done.

You can use sway or SystemD to start the proxy, your choice. 


### Using Sway

Add this line to your config:

	exec --no-startup-id mpris-proxy

### System SystemD

Add the following user unit to SystemD:

**~/.config/systemd/user/mpris-proxy.service**

	[Unit]
	Description=Forward bluetooth media controls to MPRIS

	[Service]
	Type=simple
	ExecStart=/usr/bin/mpris-proxy

	[Install]
	WantedBy=default.target

Reload daemon, enable, start, then check if it worked. 

	systemctl --user daemon-reload
	systemctl --user enable mpris-proxy.service
	systemctl --user start mpris-proxy.service
	systemctl --user status mpris-proxy.service

# References

- https://wiki.archlinux.org/index.php/MPRIS
