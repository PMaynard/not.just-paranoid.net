---
title: NationPigeon is not Paranoid
date: 2020-09-23T14:55:00Z
tags: [nationpigeon]
--- 

I've decided not to renew the domain 'nationpigeon.com'. Registered way back in late 2006, Nationpigeon was an accidental typo which became my long term presence on the Internet. 

Going forward this blog will be found under '[not.just-paranoid.net](https://not.just-paranoid.net)', all links are being redirected from nationpigeon to this new domain for the next month or so. After that, we'll have to let the world wide web do its thing. I know cool people don't [change URLs](https://www.w3.org/Provider/Style/URI.html), but there we go. 

I'll get archive.org to index this post, so if someone in the future is trying to find good ol' nationpigeon.com, they'll have some pointers. Providing archive.org is alive :) 



