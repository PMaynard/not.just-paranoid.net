---
title: Locally socket activated and containerised Riot service
date: 2020-06-17T14:50:00Z
tags: [matrix, linux, systemd]
--- 

This post details how to start a container when a port is accessed. 

Specifically, we will start an instance of the Riot IM chat service. This will let you run a version of it locally, instead of hosting it on the public internet. e.g. <https://riot.im/> 

Create the container. Note the name and port forwarding is specified here:

	podman run -d --name local_riot -p 8081:80 vectorim/riot-web

*~/.config/systemd.user/local-riot.socket*

	[Unit]
	Description=RIOT Local Socket

	[Socket]
	ListenStream=127.0.0.1:8081
	NoDelay=true
	FreeBind=true

	[Install]
	WantedBy=sockets.target

*~/.config/systemd.user/local-riot.service*

	[Unit]
	Description=RIOT Local Service

	[Service]
	Restart=always
	ExecStart=/usr/bin/podman start -a local_riot
	ExecStop=/usr/bin/podman stop -t 10 local_riot

Reload systemd, and enable, then start the socket.

	systemctl --user daemon-reload
	systemctl --user enable local-riot.socket
	systemctl --user start local-riot.socket

So now you can go to `http://127.0.0.1:8081` and use riot.
