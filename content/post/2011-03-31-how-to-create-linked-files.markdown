---
date: 2011-03-31 14:47:10
slug: how-to-create-linked-files
title: How to create linked files
categories:
- Linux
tags:
- bash
- files
- links
- Linux
---

A linked file is a file that contains a reference to another file or directory.  There are two types on links soft and hard.  A hard link can not access folders on a different volume, where as soft links can.  Here's how to create soft link files.

    
    ln -s source_file link_name
    ln -s /media/Gamma Gamma


Links are removed if the source file is removed or has no other files pointing to it.  If you want to remove the link use the **unlink** commend.
