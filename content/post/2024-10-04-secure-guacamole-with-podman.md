---
title: Secure Guacamole with Podman
date: 2024-10-04T10:00:15+01:00
---

This post covers how to setup guacamole using podman and systemd. 

## 1. Setup Guacamole

### Option A: Manual

```bash
mkdir config
podman run -p 127.0.0.1:8444:8080 -v ./config:/config:z -e EXTENSIONS="auth-quickconnect" flcontainers/guacamole
```

### Option b: Onboot

Create a quadlet config: *$HOME/.config/containers/systemd/guac.container*

```ini
[Unit]
Description=Guacamole
After=local-fs.target

[Container]
Image=flcontainers/guacamole:latest
PublishPort=127.0.0.1:8444:8080
Volume=/home/freethink/code/public/guacamole/config:/config:Z
Environment=EXTENSIONS="auth-quickconnect"
Network=pasta:-T,5910:5910,22:22

[Install]
# Start by default on boot
WantedBy=multi-user.target default.target
```

Reload systemd and start

```shell
systemctl --user daemon-reload
systemctl --user start guac.service
```

## 2. Enable TLS

Create a self signed certificate for stunnel to use:

```bash
sudo openssl req -new -out /etc/ssl/certs/stunnel.pem -keyout /etc/ssl/certs/stunnel.pem -nodes -x509 -days 365
```

Create a stunnel config: */etc/stunnel/stunnel.conf* 

```ini
debug = debug
cert = /etc/ssl/certs/stunnel.pem

[web]
accept=8445
connect=127.0.0.18444
```

Start and enable the stunnel service: 

```
sudo systemctl enable --now stunnel
```

## 3. Setup a VNC server

Create a config: *$HOME/.config/systemd/user/wayvnc.service* 

```ini
[Unit]
Description=WayVNC
After=network.target

[Service]
ExecStart=/usr/bin/wayvnc 127.0.0.1 5910
Restart=always
RestartSec=10

[Install]
WantedBy=default.target
```

Reload systemd and start vnc: 

```shell
systemctl --user daemon-reload
systemctl --user enable wayvnc.service
systemctl --user start wayvnc.service
systemctl --user status wayvnc.service
```

## 4. Configure Guacamole

1. Access via https://localhost:8445 or your [https://[external ip]:8445](https://[external ip]:8445) 
2. Login with the default `guacadmin:guacadmin` account, rename it.
3. Create a new VNC connection > Parameters > Network
   * Hostname: 127.0.0.1
   * Port: 5910

# Reference

* [Podman volumes and SELinux](https://blog.christophersmart.com/2021/01/31/podman-volumes-and-selinux/)
* [SystemD and Podman Quadlet](https://www.redhat.com/sysadmin/quadlet-podman)
* [Podman networking](https://github.com/eriksjolund/podman-networking-docs)
* [Systemd User Arch wiki](https://wiki.archlinux.org/title/Systemd/User) 
* [Guacamole Configure Connections (VNC)](https://guacamole.apache.org/doc/gug/configuring-guacamole.html#vnc)

# Appendix: Nice to have working

## VNC

It would be good to have VNC using TLS but seems to not work. Can't even get users/pass working.

Supposed to go like something like this:

Create another TLS certificate:  $HOME/.config/wayvnc

```bash
openssl req -x509 -newkey ec -pkeyopt ec_paramgen_curve:secp384r1 -sha384 -days 3650 -nodes -keyout $HOME/.config/wayvnc/tls_key.pem -out $HOME/.config/wayvnc/tls_cert.pem -subj /CN=localhost -addext subjectAltName=DNS:localhost,DNS:localhost,IP:127.0.0.1
```

Create a config: *$HOME/.config/wayvnc/config*

```ini
use_relative_paths=true
address=127.0.0.1
port=5910
enable_auth=true
username=pete
password=petel0lz
private_key_file=tls_key.pem
certificate_file=tls_cert.pem
```

Will keep an eye on [wayvnc](https://github.com/any1/wayvnc) and might update if I get it working. 

## SystemD

1. Stunnel could be run as user instead of a system service.
2. The whole chain could be started on demand via socket activation. 
3. The guac service could use WantedBy to ensure the other services are working.