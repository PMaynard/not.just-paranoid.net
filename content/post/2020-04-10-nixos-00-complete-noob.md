--- 
title: "NixOS 00: Complete NixOS n00b"
date: 2020-04-10T00:00:00Z
tags: [guide, nixos]
---

This post explain how to get NixOS up and running without getting overloaded with too many new terms.

Highlighted text is additional information that can be ignored if you only want to play with NixOS.

This post assumes you can create a virtual machine. 

> [NixOS](https://en.wikipedia.org/wiki/NixOS) is a [Linux distribution](https://en.wikipedia.org/wiki/Linux_distribution) with a fancy package manager. You manage the whole system by editing a single configuration file. The package manager takes care of installing and configuring each package and service on your system, based on your single file.
>
> This is like user dotfiles ([1](https://en.wikipedia.org/wiki/Hidden_file_and_hidden_directory),[2](https://wiki.archlinux.org/index.php/Dotfiles)) but for your whole system. 
>
> Or
>
> [Kickstart](https://en.wikipedia.org/wiki/Kickstart_(Linux)) but for everything and way more features.


# Installation

1. Download the ISO and the checksum. Then verify that its downloaded correctly.

```bash
wget https://channels.nixos.org/nixos-19.09/latest-nixos-graphical-x86_64-linux.iso{,.sha256}
sha256sum -c latest-nixos-graphical-x86_64-linux.iso.sha256
```
2. Boot up your VM with the ISO.
3. You'll be dropped into a shell.

## Configure the Disks


1. These commands create an 8GB swap partition with the remaining space used for the root partition.  
```bash
sudo parted /dev/sda -- mklabel msdos
sudo parted /dev/sda -- mkpart primary 1MiB -8GiB
sudo parted /dev/sda -- mkpart primary linux-swap -8GiB 100%
```
> Above commands are for BIOS not UEFI. Make sure your VM matches.

2. Format Filesystem.

```bash
mkfs.ext4 -L nixos /dev/sda1
mkswap -L swap /dev/sda2
```

## Base Install

1. Mount the root partition and enable the swap. 

```bash
mount /dev/disk/by-label/nixos /mnt
swapon /dev/sda2
```

2. Generate the initial configuration file. The heart of your system. This will create two files `/mnt/etc/nixos/configuration.nix` and `/mnt/etc/nixos/hardware-configuration.nix`. 
```bash
nixos-generate-config --root /mnt
```

3. To have a system with DHCP, SSH, and an admin user (freethink). Your  `configuration.nix` should look like this: 

```bash
{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];
 
  # Use the GRUB 2 boot loader. (BIOS) 
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda"; 

  networking.hostName = "nixos"; 
  networking.interfaces.ens3.useDHCP = true;

  # Set your time zone.
  time.timeZone = "Europe/Dublin";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
     wget vim htop  
  ];

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.permitRootLogin = "yes";

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.freethink = {
     isNormalUser = true;
     extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
   };

  system.stateVersion = "19.09";
}
```
4. Install this to the VM. Once completed it will ask for a root password. Then you'll be done. Enter `reboot` and remove the ISO.

```bash
nixos-install
...
reboot
```

# [Optional] Use Latest Unstable Packages  

Use the bleeding edge unstable versions.  

```bash
nix-channel --add https://nixos.org/channels/nixos-unstable nixos
nixos-rebuild switch --upgrade
```

> Note, you might be tempted to edit configuration.nix's `system.stateVersion = "19.09"`. Don't do that, it won't change your channel. This option is used to prevent major changes in packages from breaking your system. 

# Done 

Once rebooted you will be able to ssh into the machine using the root account and password. 

```bash
ssh root@ip.address 
```

Now you have a fresh install of NixOS to play with. 

> This might become a blog series. Come back soon. 

# Further Information and References

- https://nixos.org/learn.html
	- This is the 'learn' page for Nix\*. The best jumping of place to gain an understanding of everything.
- https://nixos.wiki
	- This is the NixOS wiki. Best place to get involved with the community. (Also note the Workgroups.)
- https://nixos.org/nixos/manual 
	- This the manual for NixOs. There is a lot of information in there which is worth reading over a few times. 
- https://nixos.org/nixos/options.html
  - Best way to search NixOS options.
