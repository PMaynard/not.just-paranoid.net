---
title: "dmenu and Firefox Profiles"
date: 2020-03-24T13:37:49Z
tags: [tips]
---

I use a few firefox profiles and since there is not a quick and easy profile switcher/loader addon, I've taken to using dmenu/roif to start firefox in the right profile. It will look like this: 

![](/images/posts/2020-03-23-rofi-and-firefox-profile.png)


This desktop entry will run firefox with the profile called 'work'.


*$HOME.local/share/applications/firefox-work.desktop*

```bash
#!/usr/bin/env xdg-open
[Desktop Entry]
Version=1.0
Name=Firefox Work
Type=Application
GenericName=Firefox Work Profile
Comment=Firefox but work profile
Exec=firefox -P work
Icon=firefox
Terminal=false
Type=Application
Categories=Network;WebBrowser;
Keywords=web;browser;internet;
```

The important line is: 

	Exec=firefox -P work

