---
title: "Network Audio and sndio"
date: 2018-11-18T09:47:47Z
---

[sndio](http://www.sndio.org/) is a small audio framework, part of the OpenBSD project, it has been ported to FreeBSD and Linux. Its available from most distributions' packaging systems. Sndio was created circa 2010, and is actively maintained (as of end 2018). You might consider some alternatives as PulseAudio, ALSA, JACK and OSS.

My use case for sndio is to play media (videos), and watch the action on a local machine, while the audio is being outputted on another. See the diagram below: 

![SNDIO network audio diagram](/images/posts/sndio-network-audio.svg)

I've done this with PulseAudio over bluetooth, but the video and audio were never in sync. I should try PA over network again. Anyway, here is how to do it.

# How ?

I'm using Ubuntu on my local machine, and FreeBSD on the remote side with the speakers. However, this method should be OS agnostic, and should work for any sndio supported system. 

The media is played using VLC, more on this at the end.

## Server and Speakers 

Install the needed software: 

	pkg install sndio

As root, run the aduio server, and configure it to listen on the network:

	sndiod -dd -L 0.0.0.0

Depending on your setup you'll want to have that run on boot.

## Local Machine

Install the needed software:

	apt install vlc sndio

Tell VLC which server to use, then start:

	AUDIODEVICE="snd@192.168.1.104/0" vlc

Once VLC has started, you need to tell it which audio backend to use.

	Tools -> Preferences -> Audio -> Output: Output Module: OpenBSD sndio audio output

Restart VLC, play a video and see how much in sync the audio and video are. :)

# End

I have PulseAudio running on both machines, I can have firefox running on the server playing audio from YouTube whilst sndio is doing its thing.

While this works perfectly with VLC in Ubuntu. Firefox is not built with the sndio output in the Ubuntu repositories, so kinda rubbish if I want to watch plex locally in Firefox then have the audio output on the server.

The good news is that Firefox, and many other programs have support for sndio, so not really a sndio problem, just a personal issue. *Update* If you are stuck with PA, there is a good plugin that adds sndio as output.  [pulseaudio-module-sndio](https://github.com/t6/pulseaudio-module-sndio.git).

There is a really good blog post by [Ivy Foster](https://escondida.tk/blargh/tech/sndio_on_linux/) on using sndio on Linux, which if you liked this post, I'd recommend you go check it out.

# References 

- https://www.reddit.com/r/linux/comments/3i849k/playing_around_with_openbsds_sound_server_sndio/
- http://www.sndio.org/install.html
- https://escondida.tk/blargh/tech/sndio_on_linux/
- https://wiki.voidlinux.eu/Sndio#Using_Firefox_with_Sndio.5B1.5D
- https://bugzilla.mozilla.org/show_bug.cgi?id=1351378
