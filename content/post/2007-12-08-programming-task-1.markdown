---
date: 2007-12-08
slug: programming-task-1
title: 'Programming Task #1'
categories:
- Programming
---

I have a small task for anyone who wants to learn a programming language be it Python, Java or C++.  I would like you to write a program that can:
	
* Output your name and your age on two different lines
* Output your address on multiple lines


And if you know how add some comments if not see me tomorrow.

Pete Out
