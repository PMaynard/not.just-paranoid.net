---
title: "Evince GTKRecentManager Assertion Failed"
date: 2020-01-16T13:01:38Z
tags: [help]
---

When I run evince, nothing!  
Running it via the command line gave me some more insight:

	~ > evince
	**
	Gtk:ERROR:../../../../gtk/gtkrecentmanager.c:1999:get_icon_fallback: assertion failed: (retval != NULL)                                                                                       
	Aborted (core dumped)

Appears something is messed up, probably with my GTK setup. Oh dear!

The key part I noticed was **gtk/gtkrecentmanager.c**, which seems to be talking about "GTK Recent Manager". So I cleared my current list of recent files.

	mv $HOME/.local/share/recently-used.xbel{,-bk}

Fixed. 

**EDIT: 2021 April 27**
This may be caused by firejail. I noticed a similar issue after using evince and firejail.

